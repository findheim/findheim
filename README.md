# Findheim

## Build

`$ lerna run tsc`

## Add npm package to local package

`$ lerna add REMOVE_PACKAGE_NAME --scope=LOCAL_PACKAGE`

**Install development dependency**

`$ lerna add REMOVE_PACKAGE_NAME --dev --scope=LOCAL_PACKAGE`

## Publish

First login to findheim npm registry

`$ npm login --scope=findheim`

Then simply run the "publish" npm script. It will build all packages, add versioning and publish to findheim registry

`npm run publish`

## Link package to other repository

### npm link

```
$ cd ./packages/types
$ npm link
```

In destination repository:

`$ npm link @findheim/types`
