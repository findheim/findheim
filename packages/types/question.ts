type AnwswerType = 'YESNO' | 'YESNOMAYBE' | 'MULTI' | 'RANGE' | 'NUMBER';

export interface Question {
  externalId: string;
  text: string;
  slug: string;
  answerType: AnwswerType;
  displayType: string;
  hasImportance: boolean;
  allowMultiSelection: boolean;
  icon: string;
  image: string;
  meta: any;
  shortDescription: string;
  longDescription: string;
  skippable: boolean;
  isHardFilter: boolean;
  isHardFilterOnMaxWeight: boolean;
  parentId: number;
  answers: any[];
  reply: any[];
  replies: {
    answerId: number;
  };
  questionGroup: {
    name: string;
    key: string;
  };
  subQuestions: any[];
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
}
