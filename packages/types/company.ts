export interface Company {
  id: number;
  slug: string;
  title: string;
  isSearchable: boolean;
  contact: {};
  showContactForm: boolean;
  meta: {};
  terms: string[];
  createdAt: string;
  updatedAt: string;
  deletedAt?: string;
  esType: string;
}
