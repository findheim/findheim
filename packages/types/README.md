# Typescript Definitions

> This repo contains our main typescript definitions for API responses.

## Usage

**Add types to tsconfig.json:**

```
...
"compilerOptions": {
  "typeRoots": [
    // add path to @types
    "node_modules/@types",
    "node_modules/@findheim/types",
    "./src/typings"
  ]
}
...
```

**Import in your Typescript files:**

```
import { Estate } from '@findheim/types';

const estate: Estate = ...
```
