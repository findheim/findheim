import { Company } from './company';

export type EstateType = 'apartment' | 'house';
export type OwnType = 'rented' | 'owned';
export type DataType = 'boolean' | 'number' | 'string' | 'text';

export interface Data {
  type: DataType;
  value: any;
  field: string;
  name: string;
}

export type Cost = Data & {
  format: string;
  unit: string;
  unitSymbol: string;
};

export type Price = Data & {
  format: string;
  unit: string;
  unitSymbol: string;
  field: string;
  name: string;
};

export interface Attachment {}

export interface Image {
  url: string;
  tiny: string;
  type: string;
  pHash: string;
  dominant: string;
  fileName: string;
  isCached: boolean;
  originalUrl: string;
  title: string;
  fileType: string;
  basename: string;
  firstImage?: boolean;
}

export interface Type {
  type: DataType;
  value: any;
  field: string;
  name: string;
}

export interface Feature {
  type: boolean;
  value: string;
  field: string;
  name: string;
  group?: string;
  groupName?: string;
  priority?: number;
  featureProperties?: FeatureProperties;
}

export interface FeatureProperties {
  space: number;
  quantity: number;
}

export interface Texts {
  title: string;
  object: string;
  teaser: string;
  tercet: string;
  features: string;
  location: string;
  other: string;
}

export interface Contact {
  url: string;
  agentName: string;
  telCentral: string;
  companyName: string;
  emailCentral: string;
  addressString: string;
}

export type GeoLocationType = 'APPROXIMATE' | 'ROOFTOP';
export type GeoSource = 'google';

export interface Geo {
  zip: number;
  jiLocationApproximation?: {
    lng: number;
    radius: number;
    lat: number;
  };
  country: string;
  closeTo: string;
  geocoded: {
    components: string[];
    address: string;
    lat: number;
    lng: number;
    viewport: {
      southwest: {
        lng: number;
        lat: number;
      };
      northeast: {
        lng: number;
        lat: number;
      };
    };
    bounds: {
      southwest: {
        lng: number;
        lat: number;
      };
      northeast: {
        lng: number;
        lat: number;
      };
    };
    placeId: string;
    locationType: GeoLocationType;
    source: GeoSource;
    splitAddress: {
      zip: string;
      country: string;
      district: string;
      location: string;
      politicalDistrict: string;
      street: string;
      state: string;
      houseNumber: string;
    };
  };
  addressString: string;
  location: string;
  state: string;
  floor: string;
  extended: {
    candidateScore: {
      total: number;
      dataCompleteness: number;
      nameSimilarity: number;
      googleDataOverlap: number;
      postalDistrictOverlap: number;
    };
    names: string[];
    centroid: {
      lng: number;
      lat: number;
    };
    addressComponents: {
      country: string;
      postalDistrict: string;
      district: string;
      politicalDistrict: string;
      location: string;
      state: string;
      street: string;
      houseNumber: string;
      neighborhood?: string;
    };
    ids: number[];
    orders: string[];
  };
}

export interface Meta {
  isManual: boolean;
  isCrawled: boolean;
  isOi: boolean;
  canContact: boolean;
  searchable: boolean;
  sources: string[];
  isFaved: boolean;
  isViewed: boolean;
  isSecretConfirmed?: boolean;
}

export interface Estate {
  id: number;
  slug: string;
  title: string;
  developmentId?: number;
  developmentSlug?: string;
  type: EstateType;
  ownType: OwnType;
  types: Type[];
  prices: Price[];
  spaces: Array<{
    type: DataType;
    format: string;
    value: string;
    unit: string;
    unitSymbol: string;
    field: string;
    name: string;
  }>;
  numberOfRoomsField: string;
  quantities: Array<{
    type: DataType;
    value: string;
    field: string;
    name: string;
  }>;
  features: Feature[];
  texts: Texts;
  geo: Geo;
  attachments: Attachment[];
  contact: Contact;
  availability: [];
  costs: Cost[];
  deposit: [];
  commission: [
    {
      type: DataType;
      value: string;
      field: string;
      name: string;
    },
    {
      type: DataType;
      value: string;
      field: string;
      name: string;
    }
  ];
  alignments: [];
  buildingAge: [
    {
      type: DataType;
      value: string;
      field: string;
      name: string;
    },
    {
      type: DataType;
      value: string;
      field: string;
      name: string;
    }
  ];
  conditions: [
    {
      type: DataType;
      value: string;
      field: string;
      name: string;
    }
  ];
  energyPass: {
    additionalInformation: [];
  };
  customerCommission: number;
  isSearchable: boolean;
  dataScore: number;
  adminTech: {};
  adminObject: {
    online?: boolean;
    releaseAddress: boolean;
    isSecret?: boolean;
  };
  oi: {};
  provider: {};
  ttl: number;
  dataSources: {};
  meta: Meta;
  dups: string[];
  companyId: number;
  companySlug: string;
  hostId: number;
  mainAdId: number;
  createdAtDate: string;
  createdAt: string;
  updatedAt: string;
  deletedAt?: string;
  company?: Company;
  esType: string;
  explainScore?: {
    type: {
      isExact: boolean;
      label: string;
    };
    price: {
      isExact: boolean;
      profileValue: {
        min: number;
        max: number;
      };
    };
    space: {
      isSimilar: boolean;
      profileValue: {
        min: number;
        max: number;
      };
    };
    location: {
      isSimilar: boolean;
    };
    features: {
      isExact: Array<{
        key: string;
        weight: number;
        negateKey: boolean;
        label: string;
      }>;
      isNotExact: Array<{
        key: string;
        weight: number;
        negateKey: boolean;
        label: string;
      }>;
      missing: Array<{
        key: string;
        weight: number;
        negateKey: boolean;
        label: string;
      }>;
    };
  };
  score?: {
    value: number;
    color: string;
  };
  price: {
    type: DataType;
    format: string;
    value: string;
    unit: string;
    unitSymbol: string;
    field: string;
    name: string;
    onRequest: boolean;
  };
  pricePerSqm: {
    type: DataType;
    format: string;
    value: string;
    unit: string;
    unitSymbol: string;
    field: string;
    name: string;
  };
  space: {
    type: DataType;
    format: string;
    value: string;
    unit: string;
    unitSymbol: string;
    field: string;
    name: string;
  };
  numberOfRooms: {
    type: DataType;
    value: string;
    field: string;
    name: string;
  };
  images: Image[];
  firstImage: string;
  online: boolean;
}
