import * as winston from 'winston';
const { MESSAGE, SPLAT } = require('triple-beam');
const logform = require('logform');
const tripleBeam = require('triple-beam');
const jsonStringify = require('fast-safe-stringify');

const errorHunter = logform.format(info => {
  if (info.error) return info;

  const splat = info[tripleBeam.SPLAT] || [];
  splat.find(obj => {
    if (typeof obj === 'object' && !(obj instanceof Error)) {
      const errorObj = Object.keys(obj)
        .map(key => obj[key])
        .find(o => o instanceof Error);

      info.error = errorObj;

      return errorObj;
    }

    info.error = obj;

    return obj instanceof Error;
  });

  return info;
});

const errorPrinter = logform.format(info => {
  if (!info.error) return info;

  // Handle case where Error has no stack.
  const errorMsg = info.error.stack || info.error.toString();
  info.message += `\n${errorMsg}`;

  return info;
});

const jsonFormat = winston.format((info, opts = {}) => {
  info[MESSAGE] = jsonStringify.stableStringify(
    info,
    opts.replacer,
    opts.space
  );

  return info;
});

export class Logger {
  private static _instance: Logger;
  logger: winston.Logger;
  private formats: Array<(options) => any> = [];

  constructor() {
    if (Logger._instance) {
      return Logger._instance;
    }

    this.logger = this._logger();

    Logger._instance = this;
  }

  addFormat(format: (options) => any) {
    this.formats.push(format);
  }

  private _formatter(options) {
    const payload = {
      '@timestamp': new Date().toISOString(),
      '@version': process.env.FH_VERSION,
      // tslint:disable:object-literal-key-quotes
      application: process.env.FH_APP_NAME,
      environment: process.env.FH_ENV || process.env.NODE_ENV,
      message: options.message,
      severity: options.level,
      meta: options.meta
    };

    return Object.assign({}, options, payload);
  }

  private _format() {
    const replaceError = ({ label, level, message, stack }: any) => ({
      label,
      level,
      message,
      stack
    });

    if (process.env.NODE_ENV !== 'production') {
      return winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.splat(),
        errorHunter(),
        errorPrinter(),
        winston.format.printf(info => {
          let meta = '';

          if (info instanceof Error) {
            return `${info.timestamp} ${info.level}: ${info.message}\n\n${info.stack}\n`;
          }

          if (info[SPLAT]) {
            meta = ' ' + JSON.stringify(info[SPLAT]);
          }

          return `${info.timestamp} ${info.level}: ${info.message}${meta}`;
        })
      );
    }

    return winston.format.combine(
      winston.format((info, _opts) => {
        if (Array.isArray(this.formats)) {
          for (const format of this.formats) {
            info = format(info);
          }
        }

        return info;
      })(),
      winston.format((info, opts) => {
        return this._formatter(info);
      })(),
      winston.format.errors({ stack: true }),
      winston.format.splat(),
      jsonFormat({
        replacer: (_key, value) => {
          if (value instanceof Error) {
            return replaceError(value);
          }

          return value;
        }
      })
    );
  }

  private _logger() {
    return winston.createLogger({
      exitOnError: false,
      transports: [new winston.transports.Console()],
      level: process.env.NODE_ENV === 'development' ? 'debug' : 'info',
      format: this._format()
    });
  }
}

export const logger = new Logger().logger;
